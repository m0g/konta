package db

import (
	"database/sql"

	"../model"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type Config struct {
	ConnectString string
}

func InitDb(cfg Config) (*pgDb, error) {
	if dbConn, err := sqlx.Connect("postgres", cfg.ConnectString); err != nil {
		return nil, err
	} else {
		p := &pgDb{dbConn: dbConn}
		if err := p.dbConn.Ping(); err != nil {
			return nil, err
		}
		if err := p.createTablesIfNotExist(); err != nil {
			return nil, err
		}
		if err := p.prepareSqlStatements(); err != nil {
			return nil, err
		}
		return p, nil
	}
}

type pgDb struct {
	dbConn *sqlx.DB

	sqlSelectClients *sqlx.Stmt
	sqlInsertClient *sqlx.NamedStmt
	sqlSelectClient *sql.Stmt
}

func (p *pgDb) createTablesIfNotExist() error {
	create_sql := `

       CREATE TABLE IF NOT EXISTS clients (
       id SERIAL NOT NULL PRIMARY KEY,
       first TEXT NOT NULL,
       last TEXT NOT NULL);

    `
	if rows, err := p.dbConn.Query(create_sql); err != nil {
		return err
	} else {
		rows.Close()
	}
	return nil
}

func (p *pgDb) prepareSqlStatements() (err error) {

	if p.sqlSelectClients, err = p.dbConn.Preparex(
		"SELECT id, first, last FROM clients",
	); err != nil {
		return err
	}
	if p.sqlInsertClient, err = p.dbConn.PrepareNamed(
		"INSERT INTO clients (first, last) VALUES (:first, :last) " +
			"RETURNING id, first, last",
	); err != nil {
		return err
	}
	if p.sqlSelectClient, err = p.dbConn.Prepare(
		"SELECT id, first, last FROM clients WHERE id = $1",
	); err != nil {
		return err
	}

	return nil
}

func (p *pgDb) SelectClients() ([]*model.Client, error) {
	clients := make([]*model.Client, 0)
	if err := p.sqlSelectClients.Select(&clients); err != nil {
		return nil, err
	}
	return clients, nil
}
