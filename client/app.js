import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom'

import Home from './home';
import Clients from './clients';
import HamsterUpload from './hamster-upload';

class App extends Component {
  render() {
    return (
      <main>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/clients' component={Clients}/>
          <Route path='/hamster' component={HamsterUpload}/>
        </Switch>
      </main>
    );
  }
}

export default App;
