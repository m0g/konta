import React, { Component } from 'react';
//import fetch from 'whatwg-fetch';

class HamsterUpload extends Component {
  handleFileUpload(e) {
    const data = new FormData();

    data.append('file', e.target.files[0]);

    fetch('/api/hamster-xml', {
      method: 'POST',
      body: data
    })
  }

  render() {
    return (
      <div>
        <h1>Upload your hamster XML here</h1>
        <input
          type="file"
          onChange={this.handleFileUpload.bind(this)} />
      </div>
    );
  }
}

export default HamsterUpload;
