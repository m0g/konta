#!/bin/bash

# Change language
export LANGUAGE="en_US.UTF-8"
echo "LANGUAGE="en_US.UTF-8"" | sudo tee --append /etc/default/locale
echo "LC_ALL="en_US.UTF-8"" | sudo tee --append /etc/default/locale

# Upgrade
sudo apt-get update
sudo apt-get dist-upgrade -y

# Misc packages
sudo apt-get install -y git

# Nodejs
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

# Golang
wget -cq 'https://dl.google.com/go/go1.10.linux-amd64.tar.gz'
sudo tar -C /usr/local -xzf go1.10.linux-amd64.tar.gz
rm go1.10.linux-amd64.tar.gz
export gopath=$home/.golang
export GOPATH=$GOPATH:/opt/konta
export PATH=$PATH:/usr/local/go/bin

echo 'export gopath=$home/.golang' >> ~/.bashrc
echo 'export GOPATH=$GOPATH:/opt/konta' >> ~/.bashrc
echo 'export PATH=$PATH:/usr/local/go/bin' >> ~/.bashrc

# Postgres
sudo apt-get install postgresql postgresql-contrib -y
sudo -u postgres psql --file=/opt/konta/scripts/create-user.sql

# Go Migrate
mkdir ~/bin
go get -u -d github.com/mattes/migrate/cli github.com/lib/pq
go build -tags 'postgres' -o ~/bin/migrate github.com/mattes/migrate/cli

# App
cd /opt/konta
go get "github.com/lib/pq"
go get "github.com/jmoiron/sqlx"
npm install
