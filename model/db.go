package model

type db interface {
  SelectClients() ([]*Client, error)
}
