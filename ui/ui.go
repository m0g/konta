package ui

import (
    "fmt"
    "net"
    "net/http"
    "time"
    "encoding/json"
    "strings"
    "io/ioutil"
    "encoding/xml"

    "../model"
)

type Config struct {
    Assets http.FileSystem
}

type Query struct {
  XMLName xml.Name `xml:"activities"`
  Activities []Activity `xml:"activity"`
}

type Activity struct {
  XMLName xml.Name `xml:"activity"`
  Name string `xml:"name,attr"`
  Description string `xml:"description,attr"`
  DurationMinutes int `xml:"duration_minutes,attr"`
  StartTime string `xml:"start_time,attr"`
  EndTime string `xml:"end_time,attr"`
  Tags string `xml:"tags,attr"`
}

func hamsterXMLHandler(m *model.Model) http.Handler {
  return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
    var q Query

    // in your case file would be fileupload
    file, header, err := r.FormFile("file")
    if err != nil {
      panic(err)
    }
    defer file.Close()
    name := strings.Split(header.Filename, ".")
    fmt.Printf("File name %s\n", name[0])
    byteValue, _ := ioutil.ReadAll(file)
    xml.Unmarshal(byteValue, &q)

    fmt.Println(q)

    js, err := json.Marshal(q)
    if err != nil {
      http.Error(w, err.Error(), http.StatusInternalServerError)
      return
    }

    w.Header().Set("Content-Type", "application/json")
    w.Write(js)
  })
}

func clientsHandler(m *model.Model) http.Handler {
  return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
    clients, err := m.Clients()
    if err != nil {
      http.Error(w, "This is an error", http.StatusBadRequest)
      return
    }

    js, err := json.Marshal(clients)
    if err != nil {
      http.Error(w, "This is an error", http.StatusBadRequest)
      return
    }

    fmt.Fprintf(w, string(js))
  })
}

func Start(cfg Config, m *model.Model, listener net.Listener) {

    server := &http.Server{
        ReadTimeout:    60 * time.Second,
        WriteTimeout:   60 * time.Second,
        MaxHeaderBytes: 1 << 16}

    http.Handle("/", indexHandler(m))
    http.Handle("/dist/", http.FileServer(cfg.Assets))
    http.Handle("/api/clients", clientsHandler(m))
    http.Handle("/api/hamster-xml", hamsterXMLHandler(m))

    go server.Serve(listener)
}

const indexHTML = `
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8">
    <title>Simple Go Web App</title>
  </head>
  <body>
    <div id='root'></div>
    <script type="text/javascript" src="/dist/bundle.js"></script>
  </body>
</html>
`

func indexHandler(m *model.Model) http.Handler {
  return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, indexHTML)
  })
}
